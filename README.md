# Airbnb spatial scraper

## Disclaimers

The script is developed as part of a study project in order to validate a certification. No guarantees are made about the quality of data obtained using this script, statistically or about an individual page. So please check your results.

Sometimes the Airbnb site refuses repeated requests. The script is able to use a number of proxy IP addresses to avoid being turned away. However, I am afraid that I cannot help in finding or working with proxy IP services.

## Using the script

You must be comfortable messing about with databases and python to use it.

To run the airbnb.py scraper you will need to use python 3.7 or later and install the modules listed at the top of the file.

Various parameters are stored in a configuration file, which is read in as `$USER.config`. Make a copy of `example.config` and edit it to match your database and the other parameters. The script uses proxies, so if you don't want those you may have to edit out some part of the code.

If you want to run multiple concurrent scrapes with different configuration parameters, you can do so by making a copy of your `user.config` file, editing it and running the airbnb.py scripts (see below) with an additional command line parameter. The database connection test would become

    python airbnb.py -dbp -c other.config

### Installing and/or upgrading the database schema

The airbnb.py script works with a PostgreSQL database. You need to have the PostGIS extension installed. The schema is in the file `postgresql/schema.sql`. You need to run that file to create the database tables to start with (assuming your user is named 'postgres' and database `scraper_airbnb`).


For example, if you use psql:

    psql --user postgres scraper_airbnb < postgresql/schema.sql

### Preparing to run a scrape

To check that you can connect to the database, run

    python airbnb.py -dbp


Add a search area (city) to the database:

    python airbnb.py -asa "City Name"

This adds a city to the `search_area` table.

Add a scrape description for that city:

    python airbnb.py -as "City Name"

This makes an entry in the `scrape` table, and should give you a `scrape_id` value.

### Running a scrape 

    python airbnb.py -sb scrape_id

Bounding box does a recursive geographical search, breaking a bounding box that surrounds a city into smaller pieces, and continuing to search while new listings are identified. This currently relies on adding the bounding box to the search_area table manually. A bounding box for a city can be found by entering the city at the following page:

    http://www.mapdevelopers.com/geocode_bounding_box.php

Then you can update the `search_area` table with a statement like this:

```
UPDATE search_area
SET bb_n_lat = NN.NNN,
bb_s_lat = NN.NNN,
bb_e_lng = NN.NNN,
bb_w_lng = NN.NNN
WHERE search_area_id = NNN
```

Ideally I'd like to automate this process. I am still experimenting with a combination of search_max_pages and search_max_rectangle_zoom (in the user.config file) that picks up all the listings in a reasonably efficient manner. It seems that for a city, search_max_pages=20 and search_max_rectangle_zoom=6 works well.


## Results

The basic data is in the table `room`. A complete search of a given city's listings is a "scrape" and the scrapes are tracked in table `scrape`. If you want to see all the listings for a given scrape, you can query the stored procedure scrape_room (scrape_id) from a tool such as PostgreSQL psql.

```
SELECT *
FROM room
WHERE deleted = 0
AND scrape_id = NNN
```
