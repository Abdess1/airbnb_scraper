#!/usr/bin/python3
# ============================================================================
# An ABListing represents and individual Airbnb listing
# ============================================================================
import hashlib
import logging

import psycopg2

import airbnb_logfile as ablf

logger = logging.getLogger()


class ABListing:
    """
    # ABListing represents an Airbnb room_id, as captured at a moment in time.
    # room_id, scrape_id is the primary key.
    # Occasionally, a scrape_id = None will happen, but for retrieving data
    # straight from the web site, and not stored in the database.
    """

    def __init__(self, config, room_id, scrape_id):
        self.config = config
        self.scrape_id = scrape_id  # 21
        self.room_id = room_id  # 1
        self.host_id = None  # 2
        self.address = None  # None
        self.avg_rating = None  # 10
        self.bathrooms = None  # 7
        self.bedrooms = None  # 6
        self.beds = None  # 5
        self.city = None  # 17
        self.host_languages = None  # None
        self.max_nights = None  # 14
        self.min_nights = None  # 13
        self.name = None  # None
        self.person_capacity = None  # 4
        self.reviews = None  # 8
        self.room_type = None  # 3
        self.star_rating = None  # 9
        self.superhost = None  # 11
        self.latitude = None  # 18
        self.longitude = None  # 19
        self.instant_book = None  # 12
        self.monthly_price_factor = None  # 16
        self.rate_type = None  # None
        self.weekly_price_factor = None  # 15
        self.deleted = None  # 20

        logger.setLevel(config.log_level)

    def hash_id(self, id):
        """
        GPRD : Privacy by Design
        Return a Hex digest SHA-256 hash of the given id
        """
        string_id = str(id).encode('utf8')
        hashed_id = hashlib.sha256(string_id)
        digest_id = int(hashed_id.hexdigest(), base=16)
        return digest_id

    def save_as_deleted(self):
        try:
            logger.debug("Marking room deleted: " + str(self.room_id))
            if self.scrape_id is None:
                return
            conn = self.config.connect()
            sql = """
                update room
                set deleted = 1, last_modified = now()::timestamp
                where room_id = %s
                and scrape_id = %s
            """
            cur = conn.cursor()
            cur.execute(sql, (str(self.hash_id(self.room_id)), self.scrape_id))
            if logger.isEnabledFor(logging.DEBUG):
                delete_args = str(self.hash_id(self.room_id)), self.scrape_id
            ablf.saveasjson(delete_args, step="clean_deleted", save_mode="a", ind=None)
            ablf.saveasmeta(data_name="clean_deleted")
            cur.close()
            conn.commit()
        except Exception:
            logger.error("Failed to save room as deleted")
            raise

    def save(self, insert_replace_flag):
        """
        Save a listing in the database. Delegates to lower-level methods
        to do the actual database operations.
        Return values:
            True: listing is saved in the database
            False: listing already existed
        """
        try:
            rowcount = -1
            if self.deleted == 1:
                self.save_as_deleted()
            else:
                if insert_replace_flag == self.config.FLAGS_INSERT_REPLACE:
                    rowcount = self.__update()
                if (rowcount == 0 or
                        insert_replace_flag == self.config.FLAGS_INSERT_NO_REPLACE):
                    try:
                        self.__insert()
                        return True
                    except psycopg2.IntegrityError:
                        logger.debug("Room " + str(self.room_id) +
                                     ": already scraped")
                        return False
        except psycopg2.OperationalError:
            # connection closed
            logger.error("Operational error (connection closed): resuming")
            del self.config.connection
        except psycopg2.DatabaseError as de:
            self.config.connection.conn.rollback()
            logger.erro(psycopg2.errorcodes.lookup(de.pgcode[:2]))
            logger.error("Database error: resuming")
            del self.config.connection
        except psycopg2.InterfaceError:
            # connection closed
            logger.error("Interface error: resuming")
            del self.config.connection
        except psycopg2.Error as pge:
            # database error: rollback operations and resume
            self.config.connection.conn.rollback()
            logger.error("Database error: " + str(self.room_id))
            logger.error("Diagnostics " + pge.diag.message_primary)
            del self.config.connection
        except (KeyboardInterrupt, SystemExit):
            raise
        except UnicodeEncodeError as uee:
            logger.error("UnicodeEncodeError Exception at " +
                         str(uee.object[uee.start:uee.end]))
            raise
        except ValueError:
            logger.error("ValueError for room_id = " + str(self.room_id))
        except AttributeError:
            logger.error("AttributeError")
            raise
        except Exception:
            self.config.connection.rollback()
            logger.error("Exception saving room")
            raise

    def __insert(self):
        """ Insert a room into the database. Raise an error if it fails """
        try:
            logger.debug("Values: ")
            logger.debug("\troom_id: {}".format(self.room_id))
            logger.debug("\thost_id: {}".format(self.host_id))
            conn = self.config.connect()
            cur = conn.cursor()
            sql = """
                insert into room (
                    room_id, host_id, room_type, person_capacity, beds,
                    bedrooms, bathrooms, reviews, star_rating, avg_rating,
                    superhost, instant_book, min_nights, max_nights,
                    weekly_price_factor, monthly_price_factor, city,
                    latitude, longitude, deleted, scrape_id

                )
                """
            sql += """
                values (%s, %s, %s, %s, %s,
                %s, %s, %s, %s, %s,
                %s, %s, %s, %s,
                %s, %s, %s,
                %s, %s, %s, %s
                )"""
            insert_args = (
                str(self.hash_id(self.room_id)),
                str(self.hash_id(self.host_id)),
                self.room_type, self.person_capacity, self.beds,
                self.bedrooms, self.bathrooms, self.reviews,
                self.star_rating, self.avg_rating, self.superhost,
                self.instant_book, self.min_nights, self.max_nights,
                self.weekly_price_factor, self.monthly_price_factor,
                self.city.title(), self.latitude, self.longitude,
                self.deleted, self.scrape_id
            )
            cur.execute(sql, insert_args)
            ablf.saveasjson(insert_args, step="clean_inserted", save_mode="a", ind=None)
            ablf.saveasmeta(data_name="clean_inserted")
            cur.close()
            conn.commit()
            logger.debug("Room " + str(self.room_id) + ": inserted")
            logger.debug(
                "(lat, long) = ({lat:+.5f}, {lng:+.5f})".format(lat=self.latitude, lng=self.longitude))
        except psycopg2.IntegrityError:
            # logger.info("Room " + str(self.room_id) + ": insert failed")
            conn.rollback()
            cur.close()
            raise
        except:
            conn.rollback()
            raise

    def __update(self):
        """ Update a room in the database. Raise an error if it fails.
        Return number of rows affected."""
        try:
            rowcount = 0
            conn = self.config.connect()
            cur = conn.cursor()
            logger.debug("Updating...")
            sql = """
                update room
                set host_id = %s, room_type = %s, person_capacity = %s,
                    beds = %s, bedrooms = %s, bathrooms = %s, reviews = %s,
                    star_rating = %s, avg_rating = %s, superhost = %s,
                    instant_book = %s, min_nights = %s, max_nights = %s,
                    weekly_price_factor = %s, monthly_price_factor = %s,
                    city = %s, latitude = %s, longitude = %s, deleted = %s,
                    last_modified = now()::timestamp
                where room_id = %s
                and scrape_id = %s"""
            update_args = (
                str(self.hash_id(self.host_id)),
                self.room_type, self.person_capacity, self.beds,
                self.bedrooms, self.bathrooms, self.reviews,
                self.star_rating, self.avg_rating, self.superhost,
                self.instant_book, self.min_nights, self.max_nights,
                self.weekly_price_factor, self.monthly_price_factor,
                self.city.title(), self.latitude, self.longitude,
                self.deleted,
                str(self.hash_id(self.room_id)),
                self.scrape_id,
            )
            logger.debug("Executing...")
            cur.execute(sql, update_args)
            ablf.saveasjson(update_args, step="clean_updated", save_mode="a", ind=None)
            ablf.saveasmeta(data_name="clean_updated")
            rowcount = cur.rowcount
            logger.debug("Closing...")
            cur.close()
            conn.commit()
            logger.info("Room " + str(self.room_id) +
                        ": updated (" + str(rowcount) + ")")
            return rowcount
        except:
            # may want to handle connection close errors
            logger.warning("Exception in __update: raising")
            raise
