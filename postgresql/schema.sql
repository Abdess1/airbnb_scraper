CREATE EXTENSION postgis;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;


CREATE SEQUENCE search_area_search_area_id_seq
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;

CREATE SEQUENCE scrape_scrape_id_seq
            START WITH 1
            INCREMENT BY 1
            NO MINVALUE
            NO MAXVALUE
            CACHE 1;

CREATE TABLE public.room
(
  room_id numeric(78,0) NOT NULL,
  host_id numeric(78,0),
  room_type character varying(255),
  person_capacity integer,
  beds integer,
  bedrooms integer,
  bathrooms numeric(5,2),
  reviews integer,
  star_rating double precision,
  avg_rating double precision,
  superhost boolean,
  instant_book boolean,
  min_nights integer,
  max_nights integer,
  weekly_price_factor double precision,
  monthly_price_factor double precision,
  city character varying(50),
  latitude numeric(30,6),
  longitude numeric(30,6),
  location geometry,
  deleted integer,
  last_modified timestamp without time zone DEFAULT now(),
  scrape_id integer NOT NULL DEFAULT 999999,
  CONSTRAINT room_pkey PRIMARY KEY (room_id, scrape_id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE public.schema_version
(
  version numeric(5,2) NOT NULL,
  CONSTRAINT schema_version_pkey PRIMARY KEY (version)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE public.search_area
(
  search_area_id integer NOT NULL DEFAULT nextval('search_area_search_area_id_seq'::regclass),
  name character varying(255) DEFAULT 'UNKNOWN'::character varying,
  abbreviation character varying(10),
  bb_n_lat double precision,
  bb_e_lng double precision,
  bb_s_lat double precision,
  bb_w_lng double precision,
  CONSTRAINT search_area_pkey PRIMARY KEY (search_area_id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE public.scrape
(
  scrape_id integer NOT NULL DEFAULT nextval('scrape_scrape_id_seq'::regclass),
  scrape_date date DEFAULT ('now'::text)::date,
  scrape_description character varying(255),
  search_area_id integer,
  comment character varying(255),
  scrape_method character varying(20) DEFAULT 'bounding box'::character varying,
  status smallint,
  CONSTRAINT scrape_pkey PRIMARY KEY (scrape_id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE public.scrape_progress_log_bb
(
  scrape_id integer NOT NULL,
  room_type character varying(255),
  guests integer,
  quadtree_node character varying(1024),
  last_modified timestamp without time zone DEFAULT now(),
  median_node text,
  CONSTRAINT scrape_progress_log_bb_pkey PRIMARY KEY (scrape_id)
)
WITH (
  OIDS=FALSE
);

INSERT INTO public.schema_version (version) 
  VALUES ('1.20'::numeric);

CREATE OR REPLACE FUNCTION public.trg_location()
  RETURNS trigger AS
$BODY$
BEGIN
  NEW.location := st_setsrid(
	st_makepoint(NEW.longitude, NEW.latitude),
	4326
	);
	RETURN NEW;
 END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER trg_location 
BEFORE INSERT OR UPDATE 
OF latitude, longitude ON room 
FOR EACH ROW 
    EXECUTE PROCEDURE trg_location();

ALTER SEQUENCE scrape_scrape_id_seq OWNED BY scrape.scrape_id;
ALTER SEQUENCE search_area_search_area_id_seq OWNED BY search_area.search_area_id;
